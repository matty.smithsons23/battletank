Tank Battle Game
======

Tank Battle Game Design Document (GDD):
======

**Gameplay:**
======

- Tank Battle is an open world, head to head tank combat game

- Terrain will contain cover and obstacles for tacticle advantage

- Focus on flow and feel

- winner is the player who kills the other

**Requirements:**
======


- SFX: Guns sounds, explosion, barrel moving, turret moving, tank moving

- Graphics: Simple tank made up of tracks, body, turret and barrel. textures for flare

- Background music: to add tension

 